var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync');

elixir.extend('browserSync', function () {
    /* _____________________________________________________________________________________ */
    // Add these lines to ./node_modules/laravel-elixir/ingredients/commands/CompileCss.js
    // after .pipe(gulp.dest(destination.baseDir))
    // =====================================================================================
    // .pipe(plugins.filter('**/*.css'))
    // .pipe(require('browser-sync').reload({stream: true}))
    /* _____________________________________________________________________________________ */

    var src = [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        '!public/css/*'
    ];

    gulp.task('browserSync', function () {
        if (browserSync.active === true) {
            browserSync.reload();
        } else if (gulp.tasks.watch.done === true) {
            browserSync({
                proxy: 'zahisifek.dev'
            });
        }
    });

    return this.registerWatcher('browserSync', src).queueTask('browserSync');
});

elixir(function(mix) {
    /* _____________________________________________________________________________________ */
    // modify line 91 ./node_modules/laravel-elixir/ingredients/version.js
    // return baseDir + '/build'; with return baseDir;
    /* _____________________________________________________________________________________ */
    mix
        .rubySass('main.scss', 'public/css/main.css')
        .babel([
          'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            //'bootstrap/dist/js/bootstrap.min.js',
            'sweetalert/dist/sweetalert.min.js'
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            //'bootstrap/dist/css/bootstrap.min.css',
            'sweetalert/dist/sweetalert.css',
            //'animate.css/animate.min.css'
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
        .version(['css/main.css', 'js/main.js'])
        .browserSync();
});
