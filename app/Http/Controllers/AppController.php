<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Participant;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Config;
use Carbon\Carbon;

class AppController extends Controller
{
    public function home(Request $request)
    {
        if ( ! $request->isMethod('post') && !\App::environment('local') ) {
            return redirect(config('services.facebook.app_page'));
        }
        // Fix blocked third-party cookies in Safari
        if ( count($request->cookie()) === 0 ) {
            return '<script> top.location.href = "' . route('home') . '"; </script>';
        }
        return view('home');
    }

    public function startGame()
    {
        if ( ! session('participantId') )
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $facebookId = Participant::find( session('participantId') )->value('facebook_id');

        session(['startTime' => new \DateTime() ]);
        return response()->json([ 'success' => true, 'facebookId' => $facebookId ]);
    }

    public function shareGame(Request $request)
    {
        if ( ! session('participantId') )
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::find( session('participantId') );

        FacebookSession::setDefaultApplication(
            Config::get('services.facebook.client_id'),
            Config::get('services.facebook.client_secret')
        );

        $session = new FacebookSession(Config::get('services.facebook.client_id').'|'.Config::get('services.facebook.client_secret'));

        try {
            $session->validate();
        } catch(FacebookRequestException $ex) {
            return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
        } catch(\Exception $ex) {
            return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
        }

        if ($session) {
            try {
                $response = (new FacebookRequest(
                    $session, 'GET', '/'. $request->input('id')
                ))->execute()->getResponse();

                if ($response->from->id == $participant->facebook_id) {
                    if ($participant->game_state == 1) {
                        $participant->game_state = 2;
                        if ( $participant->save() ) {
                            return response()->json([ 'success' => true, 'gameState' => $participant->game_state ]);
                        }
                    }
                }

                return response('Une erreur inconnue s\'est produite. Veuillez réessayer.', 500);

            } catch(FacebookRequestException $ex) {
                return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
            }
        }

        return response('Une erreur inconnue s\'est produite. Veuillez réessayer.', 500);
    }

    public function saveGame(Request $request)
    {
        if ( ! session('participantId') )
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::find( session('participantId') );

        $today = Carbon::today()->day;
        $lastPlayed = $participant->updated_at->day;

        if ( $participant->winner || $participant->game_state == 1 || ($participant->game_state == 3 && ($today - $lastPlayed) == 0) ) {
            return response()->json([ 'success' => false ]);
        }

        $startTime = session('startTime');
        if ( $startTime ) {
            $endTime = new \DateTime();
            $elapsedTime = $endTime->getTimestamp() - $startTime->getTimestamp();
            $participant->time_a = $elapsedTime;
        } else {
            $participant->cheater = true;
            $participant->time_a  = 0;
            return response()->json([ 'success' => true ]);
        }

        $time = base64_decode($request->input('cpn'));
        $validator = \Validator::make( ['time' => $time ], [
            'time' => 'required|numeric'
        ]);

        if ( $validator->fails() ||  $request->input('time') !== $time) {
            $participant->cheater = true;
        }

        $participant->time = $time;
        if ($time == 0) {
            $participant->game_state ++;
        } else {
            $participant->winner = true;
        }

        if ( $participant->save() ) {
            return response()->json([ 'success' => true ]);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'monoprix' => 'required',
            'card' => 'required|regex:/^22000[0-9]{8}$/|unique:participants',
            'cin' => 'required|regex:/^[0-9]{8}$/|unique:participants',
            'phone' => 'regex:/^[2549][0-9]{7}$/|unique:participants',
        ]);

        $validator->setAttributeNames([
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = session('participant');
        if ( ! $participant ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $participant->name = $request->input('firstname') . ' ' . $request->input('lastname');
        $participant->monoprix = $request->input('monoprix');
        $participant->card = $request->input('card');
        $participant->cin = $request->input('cin');
        $participant->phone = $request->input('phone');
        $participant->ip = $request->ip();


        if ( $participant->save() ) {
            session(['participantId' => $participant->id]);
            return response()->json(['success' => true ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }
}
