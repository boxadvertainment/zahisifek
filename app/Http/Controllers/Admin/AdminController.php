<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Participant;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'zahisifek') {
            return response('Unauthorized.', 401);
        }

        $participants = Participant::paginate(10);
        $count = Participant::count();
        $countWinners = Participant::where('winner', true)->count();

        return view('admin.dashboard', ['participants' => $participants, 'count' => $count, 'countWinners' => $countWinners]);
    }

}
